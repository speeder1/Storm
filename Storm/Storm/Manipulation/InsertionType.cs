﻿namespace Storm.Manipulation
{
    public enum InsertionType
    {
        BEGINNING,
        ABSOLUTE,
        LAST,
        RETURNS
    }
}